import { observer } from 'mobx-react-lite';
import React, { useEffect, useState } from 'react';
import { Spinner } from 'react-bootstrap';
import { BrowserRouter} from 'react-router-dom'
import AppRouter from './components/AppRouter';
import NavBar from './components/NavBar';
import { useStoreContext } from './contexts/storeContextProvider/context';
import { check } from './http/userAPI';

const App = observer(() => {
  const {user} = useStoreContext();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    check().then(data => {
      user.setUser(true)
      user.setIsAuth(true)
    }).finally(() => setLoading(false))
  }, [])

  if (loading){
    return <Spinner animation={"grow"}/>
  }
  
  return (
    <BrowserRouter>
      <NavBar/>
      <AppRouter/>
    </BrowserRouter>
  );
})

export default App;

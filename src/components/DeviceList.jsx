// @ts-nocheck
import { observer } from "mobx-react-lite"
import React from "react"
import { useStoreContext } from "../contexts/storeContextProvider/context"
import DeviceItem from "./DeviceItem"

const Devicelist = observer(() => {
  const {device} = useStoreContext()
  return (
    <div className="d-flex ">
        {device.devices.map((device) => 
          <DeviceItem key={device.id} device={device}/>
        )}
    </div>
  )
})

export default Devicelist
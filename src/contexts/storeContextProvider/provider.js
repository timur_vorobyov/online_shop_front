import DeviceStore from '../../store/DeviceStore';
import UserStore from '../../store/UserStrore';
import {StoreContext} from './context';
import React from 'react'

export const StoreProvider = (props) => {

  return (
    <StoreContext.Provider
      value={{
       user: new UserStore(),
       device: new DeviceStore()
      }}
    >
      {props.children}
    </StoreContext.Provider>
  );
};